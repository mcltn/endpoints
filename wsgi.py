import os, sys, json, logging, random, socket
from itertools import islice
import datetime as dt
from flask import Flask, request
import redis

application = Flask(__name__)
#application.logger.setLevel(logging.INFO)

use_ssl = False
if os.environ.get('APP_SSL') is not None:
    use_ssl = os.environ.get('APP_SSL', False)

r = None
if os.environ.get('REDIS_HOST') is not None:
    # Redis Connection
    r = redis.Redis(host=os.getenv('REDIS_HOST'), port=os.getenv('REDIS_PORT'), username="default", password=os.getenv('REDIS_PASSWORD'))

    #redis_url = os.getenv('REDIS_URL')
    #r = redis.from_url(redis_url, ssl=False)

def fib(pre=0, cur=1):
    yield pre
    while True:
        yield cur
        pre,cur=cur,pre+cur

def fibo(n):
    if n < 1:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo(n-1)+fibo(n-2)


@application.route("/", methods=['GET'])
def index():
    endpoints = []
    endpoints.append({'route':'/status','method':'GET','description':'returns a random success or failure. failures result in random 400s or 500s'})
    endpoints.append({'route':'/server','method':'GET','description':'returns hostname of the responding server instance'})
    endpoints.append({'route':'/utc','method':'GET','description':'returns current UTC time'})
    endpoints.append({'route':'/fibonacci?n=##','method':'GET','description':'returns a fibonacci sequence given a number (less than 1000), defaults to 100'})
    endpoints.append({'route':'/craps','method':'GET','description':'rolls 2 die'})
    endpoints.append({'route':'/fruits','method':'GET','description':'list of fruits'})
    endpoints.append({'route':'/vegetables','method':'GET','description':'list of vegetables'})
    endpoints.append({'route':'/badbeers','method':'GET','description':'returns list of least favored beers'})
    result = {'endpoints': endpoints}
    return json.dumps(result), 200, {'ContentType':'application/json'}

@application.route("/server", methods=['GET'])
def who():
    host = socket.getfqdn()
    return json.dumps({'host': host}), 200, {'ContentType':'application/json'}

@application.route("/status", methods=['GET'])
def random_status():
    rand = random.randint(1,20)
    if rand == 10:
        return json.dumps({'status':'error'}), 400, {'ContentType':'application/json'}
    if rand == 12:
        return json.dumps({'status':'error'}), 401, {'ContentType':'application/json'}
    if rand == 14:
        return json.dumps({'status':'error'}), 403, {'ContentType':'application/json'}
    if rand == 16:
        return json.dumps({'status':'error'}), 405, {'ContentType':'application/json'}
    if rand == 18:
        return json.dumps({'status':'error'}), 500, {'ContentType':'application/json'}

    return json.dumps({'status':'success'}), 200, {'ContentType':'application/json'}

@application.route("/utc", methods=['GET'])
def current_utc_date():
    return json.dumps({'UTC': dt.datetime.utcnow().isoformat() }), 200, {'ContentType':'application/json'}

@application.route("/fibonacci", methods=['GET'])
def fibonacci():
    fibo_str = request.args.get('n','100')
    if fibo_str.isdigit():
            num = int(fibo_str)
            if num > 999:
                return json.dumps({'error':'invalid number'}), 400, {'ContentType':'application/json'}
            else:
                fibonacci_numbers = list(islice(fib(), num))
                return json.dumps({'numbers':fibonacci_numbers}), 200, {'ContentType':'application/json'}
    return json.dumps({'error':'invalid number'}), 400, {'ContentType':'application/json'}

@application.route("/craps", methods=['GET'])
def roll_craps():
    a = random.randint(1,6)
    b = random.randint(1,6)
    ab = a + b

    switcher = {
        2: '35 to 1',
        3: '17 to 1',
        4: '11 to 1',
        5: '8 to 1',
        6: '6.2 to 1',
        7: '5 to 1',
        8: '6.2 to 1',
        9: '8 to 1',
        10: '11 to 1',
        11: '17 to 1',
        12: '35 to 1',
    }
    odds = switcher.get(ab, 'N/A')

    result = {'rolled': ab, 'dice': [a,b], 'rolledOdds': odds}
    return json.dumps(result), 200, {'ContentType':'application/json'}

@application.route("/fruits", methods=['GET'])
def fruits():
    data = []
    with open('fruits.json') as json_file:
        data = json.load(json_file)
    return json.dumps(data), 200, {'ContentType':'application/json'}

@application.route("/vegetables", methods=['GET'])
def vegetables():
    data = []
    with open('vegetables.json') as json_file:
        data = json.load(json_file)
    return json.dumps(data), 200, {'ContentType':'application/json'}

@application.route("/badbeers", methods=['GET'])
def badbeers():
    data = {}
    with open('bad_beers.json') as json_file:
        data = json.load(json_file)
    return json.dumps(data), 200, {'ContentType':'application/json'}


#######################
##  CACHE ENDPOINTS  ##
#######################
@application.route("/cache", methods=['GET'])
def get_cache():
    if r is None:
        return json.dumps({}), 400, {'ContentType':'application/json'}
    items = []
    for key in r.keys():
        value_bytes = r.get(key)
        items.append({key.decode('utf-8'): value_bytes.decode('utf-8')})
    return json.dumps(items), 200, {'ContentType':'application/json'}

@application.route("/cache/<key>", methods=['GET'])
def get_cache_by_key(key=None):
    if r is None:
        return json.dumps({}), 400, {'ContentType':'application/json'}
    items = {}
    value_bytes = r.get(key)
    item = {key.decode('utf-8'): value_bytes.decode('utf-8')}
    return json.dumps(item), 200, {'ContentType':'application/json'}

@application.route("/cache", methods=['POST'])
def add_cache():
    if r is None:
        return json.dumps({}), 400, {'ContentType':'application/json'}
    k = request.args.get('k')
    v = request.args.get('v')
    if k == None or v == None:
        return json.dumps({}), 405, {'ContentType':'application/json'}
    r.set(k,v)
    return json.dumps({k:v}), 200, {'ContentType':'application/json'}

#@application.route("/cache", methods=['PATCH'])
#def update_cache():
#    return json.dumps({'numbers':c}), 200, {'ContentType':'application/json'}



@application.route("/cache", methods=['DELETE'])
def delete_cache():
    if r is None:
        return json.dumps({}), 400, {'ContentType':'application/json'}
    for key in r.scan_iter("prefix:*"):
        r.delete(key)
    return json.dumps({}), 200, {'ContentType':'application/json'}






if __name__ == "__main__":
    if use_ssl:
        context = ('mcltn-demo-origin.cert', 'mcltn-demo-origin-private.key')
        application.run(debug=True, host='0.0.0.0', port=5000, ssl_context=context)
    else:
        application.run(debug=True, host='0.0.0.0', port=5000)
