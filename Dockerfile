FROM python:3.10-alpine

RUN apk add --update \
    python3 \
    python3-dev \
    py-pip

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install -r requirements.txt

COPY . /app

ENTRYPOINT [ "python" ]
EXPOSE 5000

CMD [ "wsgi.py" ]